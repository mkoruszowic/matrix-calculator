# -*- coding: utf-8 -*-
"""
Matrix calculator
Author: Michał Koruszowic, Beniamin Strączek
"""
import numpy as np
import time

#~~~~~~~~~~~~~~~~~~~~~~~~~~~***MATRIX DETERMINANT***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""Calculation of the matrix determinant by definition"""

def det(x):
    length=len(x)
    d={}
    sum=0
    if length>1:
        k=1
        for t in range(length):
            t2=0
            for i in range (1,length):
                d[t2]=[]  #for key t2 assign empty list
                for j in range(length):
                    if t!=j:
                        d[t2].append(x[i][j]) #attaches individual elements from the matrix to the library list
                t2=i
            x2=[d[x] for x in d] #creating a list from the library
            sum=sum+k*(x[0][t])*(det(x2))
            k=k*(-1)
            t=+1
        return sum
    else:
        return (x[0][0]) #return value of the matrix determinant

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***TRANSPOSE***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""The function creates a new list by replacing the coordinates of each item in 
the old list with i, j for j,i"""

def T(x):
    c=list()
    for i in range(len(x)):
        b=list()
        for j in range(len(x)):
            b.append(x[j][i])
        c.append(b)
    return(c)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~***COFACTOR MATRIX***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""The same operation as in the function det extended with the passage after each
 coordinate matrix and the lack of the variable sum which allowed calculating the determinant"""

def Cofactor(x):
    temp_list=[]
    Cofactor=[]
    counter=1
    length=len(x)
    d={}
    if length>1:
        k=1
        for i2 in range(0,length):
            for j2 in range(0,length):
                key=0
                for i in range (0,length):
                        if i2!=i:
                            d[key]=[]
                            for j in range(length):
                                if j2!=j:
                                        d[key].append(x[i][j])
                            key+=1
                matrix2=[d[x] for x in d]

                k=(-1)**(i2+j2) #coefficient whose value depends on the coordinates i and j
                a=k*det(matrix2)
                temp_list.append(a)
                if counter==length:
                    Cofactor.append(temp_list)
                    temp_list=[]
                    counter=0
                    k=-k
                counter+=1

        return Cofactor
    else:
        return 0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~***INVERTIBLE MATRIX***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def Inv(x):
    if det(m)==0: #rejecting the case when the determinant is equal to 0
        return "INVERTIBLE MATRIX doesn't exist"
    elif len(m)==1:
        return "You cannot do this type of calculation for the given matrix"
    else:
        c=list()
        for i in range(len(x)):
            b=list() #creating a new list b in which the calculated inverse matrix elements will be saved
            for j in range(len(x[0])):
                b.append(round(float(Cofactor(T(x))[i][j]*(1/det(m))),2)) #rounding the number and adding it to the list
            c.append(b) #adding to list c the list b consisting of a row of the inverse matrix
        return(c)

#~~~~~~~~~~~~~~~~~~~~~~~~~***CREATING NEW MATRIX***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def matrix(n):
    y=[]
    z=[]
    for i in range(1,n+1):
        for j in range(1,n+1):
            while 1:
                try:
                    x=int(input("Enter number of coordinates (%s,%s): " %(str(i),str(j))))
                    break
                except:
                    print("\nPlease enter a number!")
            y.append(x)
            if j==n:
                z.append(y)
                y=[]
    print("\nYour matrix:\n")
    printing(z)
    return(z)

#~~~~~~~~~~~~~~~~~~***MATRIX DETERMINAT TIME COUNTER***~~~~~~~~~~~~~~~~~~~~~~~~~~

"""Function counting the execution time for the built-in function linalg.det and the det function"""

def timer():
    start = time.clock() #variable to which time is assigned when the function is called
    g = np.linalg.det(m)
    end = time.clock() #variable to which time is assigned when the function ends
    time1 = end - start #counting the difference between the beginning and the end of the function call

#same as above
    start = time.clock()
    h = det(m)
    end = time.clock()
    time2 = end - start #counting the difference between the beginning and the end of the function call
    print("linalg.det():")
    print("RESULT: %i    CALCULATION TIME: %f [s] \n" %(g,time1))
    print("det():")
    print("RESULT: %i    CALCULATION TIME: %f [s] \n" %(h,time2))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***NEW MATRIX***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""The function creates the matrix according to the user's indications"""
def size():
    while 1:
        try:
            n=int(input("\nEnter the size of the square matrix: "))
            while n<=0: #loop capturing the negative matrix
                n=int(input("The number must be positive. \nEnter the size of the square matrix: "))
            break
        except: #protection against giving the string
            print("\nPlease enter a number!")
    return n

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***DISPLAY***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""A function that prints a list so that it looks like a matrix"""
def printing(x):
    if x==0:
        print(x)
    elif type(x)==str:
        print(x)
    else:
        for i in range(len(x)):
            print(x[i]) #printing the i-list

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~***MAIN SECTION***~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

while 1:
    try:
        n=int(input("WELCOME TO THE MATRIX CALCULATOR! \nPlease enter the size of the square matrix: "))
        while n<=0:
            n=int(input("The number must be positive. \nEnter the size of the square matrix: "))
        break
    except:
        print("\nPlease enter a number!")


m=matrix(n) #calling matrix forming function


while 1:
    print(2*"\n")
    print("Available operations: \n")
    print("(1) Determinant of a matrix")
    print("(2) Inverse of a matrix")
    print("(3) Transpose of a matrix")
    print("(4) Calculating cofactor matrix")
    print("(5) Calculating adjugate matrix")
    while 1: #protection in case of providing a string
        try:
            n=int(input("Choose an action: "))
            break
        except:
            print("\nPlease enter a number!")
    print("\n")

    while n>=6: #checking if given number is out of range
        while 1:
            try:
                n=int(input("Number out of range. Choose an action: "))
                break
            except:
                print("\nPlease enter a number!")

    """Performing the previously selected action"""

    if n==1:
        print("MATRIX DETERMINANT~~~~~~~~~~~~~~~~~~~~~~\n")
        timer()
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    elif n==2:
        print("INVERTIBLE MATRIX")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        printing(Inv(m))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    elif n==3:
        print("TRANSPOSE")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        printing(T(m))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    elif n==4:
        print("COFACTOR MATRIX")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        printing(Cofactor(m))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    elif n==5:
        if len(m)!=1:

            print("ADJUGATE MATRIX")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            printing(T(Cofactor(m)))
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        else:
            print("ADJUGATE MATRIX")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("You cannot do this type of calculation for the given matrix")
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")




    print("\nDo you want to keep going?")
    print("(1) Yes, on the current matrix")
    print("(2) Yes, on the new matrix")
    print("(3) No")
    while 1:  #checking if given number is out of range
        try:
            n=int(input("Odp.: "))
            while n!=1 and n!=2 and n!=3:
                n=int(input("Please enter a number 1, 2 or 3. Answer.: "))
            break
        except:  #protection in case of providing a string
            print("\nPlease enter a number!")
    if n==1:
        pass
    elif n==2:
        m=matrix(size())
    elif n==3:
        break
